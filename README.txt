Commerce Shipping province Weight
====================================

Shipping rate calculations by province & weight for 
[Drupal Commerce](http://drupal.org/project/commerce).

---

Features
--------
  Add Shipping rate based on Province and weight of the order, Example 1kg to New york cost 10$, same 1Kg to California cost 15$, You can configure per country. 

